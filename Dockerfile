FROM node:15.8.0-alpine3.10

RUN mkdir -p /app
WORKDIR /app

COPY . /app
RUN npm install

# Check the app for any syntax errors
# RUN node app.js

EXPOSE 8080

CMD ["npm", "start"]
