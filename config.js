let config = {
    version: process.env.VERSION || "laptop",
    port: process.env.PORT || 8080,
    from: process.env.FROM || 'laptop',
    endpoint_host: process.env.ENDPOINT_HOST || 'http://localhost:8080'
}

module.exports = config